from django.shortcuts import render
from django.contrib.auth.decorators import login_required
# Create your views here.
from django.views.generic import TemplateView


#@login_required
def CasaPageView(request):
    variable = "holamundo"
    return render(request,'home.html', {'variable':variable})
class HomePageView(TemplateView):
    template_name = "home.html"


def AboutPageView(request):
    variable = "ABOUT"
    return render(request,'about.html', {'variable':variable})
# class AboutPageView(TemplateView):
#     template_name = "inicio/about.html"


#def LogIn(request):
#    variable = "Login"
#    return render(request,'login.html', {'variable':variable})

@login_required
def NadaPageView(request):
    variable = "NADA"
    return render(request,'nada.html', {'variable':variable})

#   Panxo:
@login_required
def inicio(request):
    user = request.user
    if user.groups.filter(name='Administrador').exists():
        # Tiene los privilegios de este grupo
        return render(request, 'lgdin_admin.html')
    
    elif user.groups.filter(name='Funcionario').exists():
        # Redireccionar, levantar un error, etc.
        return render(request, 'lgdin_funcionario.html')
    
    elif user.groups.filter(name='Jefe').exists():
        # Redireccionar, levantar un error, etc.
        return render(request, 'lgdin_jefe.html')
    
    elif user.groups.filter(name='Subjefe').exists():
        # Redireccionar, levantar un error, etc.
        return render(request, 'lgdin_subjefe.html')
    
    elif user.groups.filter(name='Subadmin').exists():
        # Redireccionar, levantar un error, etc.
        return render(request, 'lgdin_subadmin.html')
    
    else:
        # Redireccionar, levantar un error, etc.
        variable = "NADA"
        return render(request,'nada.html', {'variable':variable})

# @login_required
# def LogedInView(request):
#     # Obtener el rol de usuario
#     user_role = request.user.role  # Asumiendo que hay un atributo "role" en el modelo de usuario
    
#     context = {
#         'user_role': user_role
#     }
    
#     return render(request, 'mi_plantilla.html', context)