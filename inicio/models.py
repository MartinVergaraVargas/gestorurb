from django.db import models
from django.contrib.auth.models import AbstractUser
from .manager import UserManager

class User(AbstractUser):
    username = None
    email = models.EmailField(unique=True)
    funcion = models.CharField(max_length=50)
    
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

# from django.contrib.auth.models import AbstractUser

# class CustomUser(AbstractUser):
#     # Agrega un campo para el rol
#     ROLE_CHOICES = [
#         ('admin', 'Administrador'),
#         ('project_manager', 'Jefe de proyecto'),
#         ('sub_admin', 'Sub-administrador'),
#         ('sub_project_manager', 'Sub-jefe de proyecto'),
#         ('employee', 'Funcionario'),
#     ]
#     role = models.CharField(max_length=20, choices=ROLE_CHOICES)
