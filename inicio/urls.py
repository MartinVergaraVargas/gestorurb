# inicio/urls.py
from django.urls import path
from .views import HomePageView, AboutPageView, CasaPageView, NadaPageView

urlpatterns = [
    #path("", HomePageView.as_view(), name="home"),
    #path("about/:", AboutPageView.as_view(), name="about"),
    path("", CasaPageView, name="home"),
    #path("about/", AboutPageView.as_view(), name="about"),
    path("about/", AboutPageView, name="about"),
    path("nada/", NadaPageView, name="nada"),
]